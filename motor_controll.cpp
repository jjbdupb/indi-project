#include "motor_controll.hpp"

using namespace cv;

int get_nSteps(double num){
	return floor(abs(num)/params.PIXEL_CONVERSION);

}

void controll_motor(Point dxdy){
	
	double dx = dxdy.x;
	double dy = dxdy.y;

	if(calc_dist_line(dx, dy) < params.MOTOR_LINE_THRESH) return;

	int m1_dir;
	int m2_dir;

	if(dx>0){
		m1_dir = 1;
	}else{
		m1_dir = 0;
	}
	if(dy>0){
		m2_dir = 1;
	}else{
		m2_dir = 0;
	}

	int m1_step = get_nSteps(dx);
	int m2_step = get_nSteps(dy);

	setupGPIO();

	stepMotor(1, m1_dir, m1_step);
	stepMotor(2, m2_dir, m2_step);

	teardownGPIO();

}
