#ifndef __GPIO__
#define __GPIO__

#include <pigpio.h>
#include <unistd.h>
#include <iostream>
#include "params.hpp"

extern const ConfigParams params;

void setupGPIO();

void stepMotor(int motor, int dir, int num);

void setIOPin(int pin, int logic);

void teardownGPIO();

#endif