#ifndef __HELPER__
#define __HELPER__

#include <vector>
#include <opencv2/opencv.hpp>
#include <opencv2/imgproc.hpp>
#include "params.hpp"

extern const ConfigParams params;

using namespace cv;

/*
 *@breif plots the lines defined by co_ords onto the given frame
 *@param frame : the frame object to plot lines onto
 *@param co_ords : the co-ordinates that define the line to plot
*/
void plot_line(Mat frame, Vec4f co_ords, Point origin);

/*
 *@breif calculates the mean line from a vector of lines
 *@param lines: the vector of lines to calculate the mean of
 *@return co_ords : co-ordinates defining the mean line
*/
Vec4f calc_mean_lines(std::vector<Vec4f> lines);


double calc_dist_line(double dx, double dy);

/*
 *@breif calculates the distance between two points
 *@param p1: point 1
 *@param p2: point 2
 *@return dist: the distance between p1 and p2
*/
double calc_dist_points(Point p1, Point p2);

/*
 *@breif test to see if a vector is not zero at all points
 *@param vec: vector to check
 *@return bool: true if vector contains at least one non-zero element
*/
bool vec_not_zero(Vec4f vec);

/*
 *@breif print a vector of Vec4f objects to cout
 *@param v: vector to proint
*/
void print_lines(std::vector<Vec4f> v);

/*
 *@breif translates the co-ordinates of a line to be reletive of the origin provided
 *@param p: lines to translate defined by points in Vec4f format
 *@param origin: the origin point to translate around
 *@return p: the line now relative to the origin point
*/
Vec4f translate_co_ords(Vec4f p, Point origin);

#endif