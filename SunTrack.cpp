#include <iostream>
#include <opencv2/opencv.hpp>
#include "frame_controll.hpp"
#include "motor_controll.hpp"
#include "params.hpp"

using namespace cv;
extern const ConfigParams params;

int main(int argc, char const *argv[])
{
	int deviceID = 0;
	int apiID = cv::CAP_ANY;

	std::cout << "starting frame grabbing" << std::endl;

	Point origin = get_frame_origin(deviceID, apiID);

	std::cout<<"Origin: " << origin << std::endl;

	for(;;){
		Point dxdy = compute_avg_dxdy(deviceID, apiID, origin);
		if(!isnan(dxdy.x)){
			if(params.DEBUG) std::cout << dxdy<< std::endl;
			controll_motor(dxdy);
		}
	}

	return 0;
}