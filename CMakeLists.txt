cmake_minimum_required(VERSION 3.10)

project(SunTrack VERSION 0.1.0)

set(CMAKE_CXX_STANDARD 11)
set(CMAKE_CXX_STANDARD_REQUIRED True)

find_package(OpenCV REQUIRED)
find_package(Threads REQUIRED)
list(APPEND CMAKE_MODULE_PATH "${CMAKE_CURRENT_LIST_DIR}/cmake")
find_package(pigpio REQUIRED)


add_executable(SunTrack SunTrack.cpp frame_controll.cpp helper.cpp img_process.cpp gpio.cpp motor_controll.cpp params.cpp)

target_include_directories(SunTrack PUBLIC
						   "${PROJECT_SOURCE_DIR}"
						   
						   ${OPENCV_INCLUDE_DIRS}
						   ${pigpio_INCLUDE_DIRS})

target_link_libraries(SunTrack ${OpenCV_LIBS} ${pigpio_LIBRARY})
