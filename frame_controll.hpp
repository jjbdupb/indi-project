#ifndef __FRAME_CONTROLL__
#define __FRAME_CONTROLL__

#include <iostream>
#include <opencv2/opencv.hpp>
#include <opencv2/imgproc.hpp>
#include "img_process.hpp"
#include "helper.hpp"
#include "params.hpp"

extern const ConfigParams params;

/*
 *@breif Finds the origin point of the frame. Origin is defined as the location of the centre shadow casting rod
 *@param deviceID : the ID of the video device to open
 *@param apiID: the ID of the video api used to capture frames from the video device
 *@return origin: the co-ordinates of the origin of the frame
*/
Point get_frame_origin(int deviceID, int apiID);

/*
 *@breif compute the average dx and dy values for the shadow over FRAME_AVG frames
 *@param deviceID : the ID of the video device to open
 *@param apiID: the ID of the video api used to capture frames from the video device
 *@param origin: the origin Point of the image, used to calculate dx and dy values relative to center rod
 *@return dxdy: point containg the average dx and dy values of the shadow over FRAME_AVG
*/
Point compute_avg_dxdy(int deviceID, int apiID, Point origin);

#endif