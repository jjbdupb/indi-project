#ifndef __IMG_PROC__
#define __IMG_PROC__

#include <iostream>
#include <opencv2/opencv.hpp>
#include <opencv2/imgproc.hpp>
#include "helper.hpp"
#include "params.hpp"

extern const ConfigParams params;

using namespace cv;

/*
 * @breif function to detect all lines within image
 * @param frame : Image object to detect lines within
 * @return lines : a vector of Vec4f containing the points that define the line | <x1,y1,x2,y2> 
*/
std::vector<Vec4f> line_detect(Mat frame);

/*
 * @breif calculates and returns the modal average value from a Mat object
 * @param frame: mat object to find the mode of
 * @return mode: modal value of the Mat object
*/
double get_mode_value(Mat frame);

/*
 * @breif runs a binary filter on a given image, theashold value is determaned by the modal average in the image
 * @param frame: Mat object to filter
 * @param mode: mode the filter should operate in | 0 for standard use | 1 for origin use
 * @return frame: Mat object of the binary filtered image
*/
Mat binary_filter(Mat frame, int mode);

/*
 * @breif function to find the co-ords of the center rod in the frame.
 * @param frame: Mat object to find the center rod in
 * @return origin: Point object of the center rod.
*/
Point get_origin(Mat frame);

/*
 * @breif Main image processing function to find the co-ordinates of the shadows line
 * @param frame: Mat object to process
 * @return co_ords: the co-ordinates that define the shadow line detected in the image.
*/
Vec4f process(Mat frame);

#endif