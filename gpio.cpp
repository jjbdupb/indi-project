#include "gpio.hpp"

void setupGPIO(){
	int init = gpioInitialise();
	if(init<0){
		std::cerr<<"ERROR GPIO FAILED TO INITALISE" << std::endl;
		exit(-1);
	}

	gpioSetMode(params.M1STEP, PI_OUTPUT);
	gpioSetMode(params.M1DIR, PI_OUTPUT);
	gpioSetMode(params.M2STEP, PI_OUTPUT);
	gpioSetMode(params.M2DIR, PI_OUTPUT);
}

void stepMotor(int motor, int dir, int num){
	int stepPin;
	int dirPin;
	if(motor == 1){
		stepPin = params.M1STEP;
		dirPin = params.M1DIR;
	}else if(motor == 2){
		stepPin = params.M2STEP;
		dirPin = params.M2DIR;
	}else{
		std::cerr << "ERROR MOTOR NUM NOT RECOGNISED" << std::endl;
		exit(-1);
	}
	if(dir == 0 || dir == 1){
		gpioWrite(stepPin, 0);
		gpioWrite(dirPin, dir);
		for (int i = 0; i < num; ++i)
		{
			gpioWrite(stepPin, 1);
			usleep(params.PULSE_WIDTH_MS*500);
			gpioWrite(stepPin, 0);
			usleep(params.PULSE_WIDTH_MS*500);
		}
		gpioWrite(stepPin, 0);
	}else{
		std::cerr << "ERROR DIRECTION NOT RECOGNISED" << std::endl;
		exit(-1);
	}
}

void setIOPin(int pin, int logic){
	gpioWrite(pin, logic);
}

void teardownGPIO(){
	gpioTerminate();
}

