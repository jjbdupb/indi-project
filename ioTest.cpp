#include <iostream>
#include "gpio.hpp"
#include <unistd.h>


int main(int argc, char const *argv[])
{
	setupGPIO();
	
	int num = 6;
	int dir;
	for(int i = 0; ; i++){ 
	dir = i%2;		
	stepMotor(1, dir, num);

	}
	gpioTerminate();	

	
	return 0;
}
