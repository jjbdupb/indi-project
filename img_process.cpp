#include "img_process.hpp"

using namespace cv;

std::vector<Vec4f> line_detect(Mat frame){
	Canny(frame, frame, 50, 200, 3);

	std::vector<Vec4f> lines;

	HoughLinesP(frame, lines, 2, CV_PI/180, params.LINE_THRESH, params.MIN_LINE_LENGTH, params.MAX_LINE_GAP);
    return lines;

}

double get_mode_value(Mat frame){
	Mat grey_hist;
	float range[] = { 0, 256 } ; //the upper boundary is exclusive
	const float* histRange = { range };

	calcHist(&frame, 1, 0, Mat(), grey_hist, 1, &params.HIST_SIZE, &histRange, true, false);
	Point max;
	minMaxLoc(grey_hist, NULL, NULL, NULL, &max);
	return max.y;

}

Mat binary_filter(Mat frame, int mode){
	cvtColor(frame, frame, COLOR_BGR2GRAY);

	double threshval;
	if(params.DISPLAY_IMAGE) imshow("Grey", frame);
	
	if(mode==0){
		threshval = get_mode_value(frame)*params.MODAL_INCREASE;
	}else if(mode==1){
		threshval = get_mode_value(frame)*params.ORIGIN_THRESH;
	}else{
		std::cerr<<"UNKNOWN BINARY FILTER MODE" << std::endl;
		exit(-1);
	}
	threshold(frame, frame, threshval, 255, THRESH_BINARY);

	return frame;
}

Point get_origin(Mat frame){
	frame = binary_filter(frame, 1);
	if(params.DISPLAY_IMAGE) imshow("origin", frame);
	bitwise_not(frame, frame);
	Moments m = moments(frame, true);
	Point origin = Point(m.m10/m.m00, m.m01/m.m00);
	return origin;
}


Vec4f process(Mat frame){
	if (frame.empty()){
		std::cerr <<"ERROR! empty frame grabbed\n";
		exit(-1);
	}

	frame = binary_filter(frame, 0);

	if(params.DISPLAY_IMAGE) imshow("binary", frame);

	std::vector<Vec4f> lines = line_detect(frame);

	Vec4f co_ords = calc_mean_lines(lines);

	return co_ords;

}