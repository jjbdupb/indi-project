#include "params.hpp"

const ConfigParams params("suntrack.conf");

ConfigParams::ConfigParams(const std::string &fileName)
:DISPLAY_IMAGE(loadBool(fileName, "GENERAL", "DISPLAY_IMAGE", false))
,DEBUG(loadBool(fileName, "GENERAL", "DEBUG", false))
,PIXEL_CONVERSION(loadInt(fileName, "MotorControll", "PIXEL_CONVERSION", 4))
,FRAME_AVG(loadInt(fileName, "FrameControll", "FRAME_AVG", 10))
,LINE_THRESH(loadInt(fileName, "ImgProccess", "LINE_THRESH", 50))
,MIN_LINE_LENGTH(loadInt(fileName, "ImgProccess", "MIN_LINE_LENGTH", 50))
,MAX_LINE_GAP(loadInt(fileName, "ImgProccess", "MAX_LINE_GAP", 10))
,HIST_SIZE(loadInt(fileName, "ImgProccess", "HIST_SIZE", 256))
,MODAL_INCREASE(loadFloat(fileName, "ImgProccess", "MODAL_INCREASE", 0.75f))
,ORIGIN_THRESH(loadFloat(fileName, "ImgProccess", "ORIGIN_THRESH", 1.5f))
,M1STEP(loadInt(fileName, "GPIO", "M1STEP", 25))
,M1DIR(loadInt(fileName, "GPIO", "M1DIR", 27))
,M2STEP(loadInt(fileName, "GPIO", "M2STEP", 23))
,M2DIR(loadInt(fileName, "GPIO", "M2DIR", 24))
,MOTOR_LINE_THRESH(loadFloat(fileName, "MotorControll", "MOTOR_LINE_THRESH", 100))
,PULSE_WIDTH_MS(loadInt(fileName, "GPIO", "PULSE_WIDTH_MS", 10))
{
}

int loadInt(const std::string &fileName, const std::string paramLoc, const std::string paramName, int defautlVal){
	INIReader reader(fileName);
    if (reader.ParseError() != 0) {
        std::cout << "Can't load config file\n";
        exit(-1);
    }

    return reader.GetInteger(paramLoc, paramName, defautlVal);
}

float loadFloat(const std::string &fileName, const std::string paramLoc, const std::string paramName, float defautlVal){
	INIReader reader(fileName);
    if (reader.ParseError() != 0) {
        std::cout << "Can't load config file\n";
        exit(-1);
    }

    return reader.GetFloat(paramLoc, paramName, defautlVal);
}

bool loadBool(const std::string &fileName, const std::string paramLoc, const std::string paramName, bool defautlVal){
	INIReader reader(fileName);
    if (reader.ParseError() != 0) {
        std::cout << "Can't load config file\n";
        exit(-1);
    }

    return reader.GetBoolean(paramLoc, paramName, defautlVal);
}

