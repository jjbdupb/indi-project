#include "frame_controll.hpp"

using namespace cv;

Point get_frame_origin(int deviceID, int apiID){
	Mat frame;
	VideoCapture cap;

	cap.open(deviceID, apiID);

	if (!cap.isOpened()){
		std::cout <<"Error Cannot open Video" << std::endl;
		exit(-1);
	}

	cap.read(frame);

	Point origin = get_origin(frame);

	return origin;

}


Point compute_avg_dxdy(int deviceID, int apiID, Point origin){
	Mat frame;
	VideoCapture cap;

	cap.open(deviceID, apiID);

	if (!cap.isOpened()){
		std::cout <<"Error Cannot open Video" << std::endl;
		exit(-1);
	}
	std::vector<Vec4f> lines;
	for (int i = 0; i < params.FRAME_AVG; ++i)
	{
		cap.read(frame);

		Vec4f co_ords = process(frame);
		if(vec_not_zero(co_ords)){
			lines.push_back(co_ords);
		}
		if (waitKey(5) >=0)
			exit(0);
	}
	Vec4f l = calc_mean_lines(lines);

	origin = get_origin(frame);

	if(params.DISPLAY_IMAGE) plot_line(frame, l, origin);

	l = translate_co_ords(l, origin);
	Point p1 = Point(l[0], l[1]);
	Point p2 = Point(l[2], l[3]);
	double dx, dy;

	if(calc_dist_points(p1, Point(0,0)) < calc_dist_points(p2, Point(0,0))){
		dx = p2.x - p1.x;
		dy = p2.y - p1.y;
	}else{
		dx = p1.x - p2.x;
		dy = p1.y - p2.y;
	}

	Point dxdy = Point(dx, dy);

	return dxdy;

}