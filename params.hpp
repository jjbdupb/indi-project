#ifndef __PARAMS__
#define __PARAMS__

#include "dependency/INIReader.h"
#include <iostream>

struct ConfigParams
{
	const int PIXEL_CONVERSION;
	const int LINE_THRESH;
	const int MIN_LINE_LENGTH;
	const int MAX_LINE_GAP;
	const int HIST_SIZE;
	const float MODAL_INCREASE;
	const float ORIGIN_THRESH;
	const int M1STEP;
	const int M1DIR;
	const int M2STEP;
	const int M2DIR;
	const int PULSE_WIDTH_MS;
	const int FRAME_AVG;
	const float MOTOR_LINE_THRESH;
	const bool DISPLAY_IMAGE;
	const bool DEBUG;

	ConfigParams(const std::string &fileName);

};

extern const ConfigParams params;

int loadInt(const std::string &fileName, const std::string paramLoc, const std::string paramName, int defautlVal);

float loadFloat(const std::string &fileName, const std::string paramLoc, const std::string paramName, float defautlVal);

bool loadBool(const std::string &fileName, const std::string paramLoc, const std::string paramName, bool defautlVal);



#endif