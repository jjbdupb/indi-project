#include "helper.hpp"

using namespace cv;

void plot_line(Mat frame, Vec4f co_ords, Point origin){
	line(frame, Point(co_ords[0], co_ords[1]), Point(co_ords[2], co_ords[3]), Scalar(0,0,255), 3, LINE_AA);
	circle(frame, origin, 3, Scalar(0,255,0));
	imshow("lines", frame);
}

Vec4f calc_mean_lines(std::vector<Vec4f> lines){
	Vec4f co_ords;

	for (int i = 0; i < lines.size(); ++i){
		Vec4f l = lines[i];
		for (int j = 0; j < 4; ++j)
		{
			co_ords[j] += l[j];
		}
	}


	if(lines.size()>0){
		double scale = 1.0/lines.size();
		co_ords = co_ords * scale;
	}
	return co_ords;
}

double calc_dist_line(double dx, double dy){
	return sqrt(dx*dx +dy*dy);
}

double calc_dist_points(Point p1, Point p2){
	double dx = p2.x - p1.x;
	double dy = p2.y - p1.x;
	return calc_dist_line(dx, dy);
}

bool vec_not_zero(Vec4f vec){
	for (int i = 0; i < 4; ++i)
	{
		if (vec[i] != 0)
			return true;
	}

	return false;
}

void print_lines(std::vector<Vec4f> v){
	for (int i = 0; i < v.size(); ++i)
	{
		std::cout<<v[i]<<std::endl;
	}
}

Vec4f translate_co_ords(Vec4f p, Point origin){
	p[0] = p[0] - origin.x;
	p[1] = origin.y - p[1];
	p[2] = p[2] - origin.x;
	p[3] = origin.y - p[3];

	return p;

}
