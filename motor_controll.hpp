#ifndef __MOTOR_CONTROLL__
#define __MOTOR_CONTROLL__

#include "gpio.hpp"
#include "params.hpp"
#include "helper.hpp"
#include <iostream>
#include <opencv2/opencv.hpp>

#include <cmath>

extern const ConfigParams params;

using namespace cv;

int get_nSteps(double num);

void controll_motor(Point dxdy);


#endif
