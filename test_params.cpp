#include "params.hpp"
#include <iostream>

extern const ConfigParams params;

int main(int argc, char const *argv[])
{
	std::cout<<params.LINE_THRESH << std::endl;
	return 0;
}